<?php

spl_autoload_register(function($class){
   if(file_exists("entidades/".$class.".php")){
       include "entidades/".$class.".php";
   }
});

//Create a warrior
$warrior = CharacterFactory::getCharacterWarrior("Warrior", "humano", 100, 50, 50, 0, 25);

//Create a Mage
$mage = CharacterFactory::getCharacterMage("Mage", "elfo", 100, 50, 10, 50, 15);

//Create a Rogue
$rogue = CharacterFactory::getCharacterRogue("Rogue", "hombre lobo", 100, 10, 20, 3, 50);

// The Warrior fight the mage
$warrior->attack($mage);

// The Mage fight the Rogue
$mage->attack($rogue);

// The Rogue fight the Warrior
$Rogue->attack($warrior);
     
// falencias observadas en las entidades en cuanto a programación orientada objetos