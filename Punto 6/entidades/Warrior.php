<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Warrior
 *
 * @author pabhoz
 */
class Warrior {
    //put your code here
    private $nombre, $raza, $hp, $mn, $str, $md, $ag; 
    private $clase = "Warrior";
    
    function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        //el constructor no es el adecuado
        $this->nombre = $nombre;
        $this->raza = $raza;
        $this->hp = $hp;
        $this->mn = $mn;
        $this->str = $str;
        $this->md = $md;
        $this->ag = $ag;
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getRaza() {
        return $this->raza;
    }

    function getHp() {
        return $this->hp;
    }

    function getMn() {
        return $this->mn;
    }

    function getStr() {
        return $this->str;
    }

    function getMd() {
        return $this->md;
    }

    function getAg() {
        return $this->ag;
    }

    function getClase() {
        return $this->clase;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setRaza($raza) {
        $this->raza = $raza;
    }

    function setHp($hp) {
        $this->hp = $hp;
    }

    function setMn($mn) {
        $this->mn = $mn;
    }

    function setStr($str) {
        $this->str = $str;
    }

    function setMd($md) {
        $this->md = $md;
    }

    function setAg($ag) {
        $this->ag = $ag;
    }

    function setClase($clase) {
        $this->clase = $clase;
    }

    public function attack($character) {
        $tipo=$character->getClase();
        if($tipo=="Rogue"){
            $dano=$this->getStr()*2.5;
            return $this->getNombre()." ataca a ".$character->getNombre()." causando ".$dano." de daño";
            getHurt($dano);
        } else {
            $dano=$this->getStr()*0.6;
            return $this->getNombre()." ataca a ".$character->getNombre()." causando "." de daño";
            getHurt($dano);
        }        
    }
    
    public function getHurt($hp) {
        $dano=$this->getHp()-$hp;
        if($dano<=0){
         dramaticDeath();   
        }  
    }
    public function dramaticDeath() {
        return "Arrrg! yo ".$this->getNombre()." el gran guerrero, he sido derrotado en batalla.";
    }

}
//los comentarios básicamente serán los mismos que los anteriores