<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Character
 *
 * @author pabhoz
 */
class Character {
    //put your code here
    private $nombre, $raza, $hp, $mn, $str, $md, $ag; 
    private $clase = "normal";
    
    function __construct($nombre, $raza, $hp, $mn, $str, $md, $ag) {
        $this->nombre = $nombre;
        $this->raza = $raza;
        $this->hp = $hp;
        $this->mn = $mn;
        $this->str = $str;
        $this->md = $md;
        $this->ag = $ag;
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getRaza() {
        return $this->raza;
    }

    function getHp() {
        return $this->hp;
    }

    function getMn() {
        return $this->mn;
    }

    function getStr() {
        return $this->str;
    }

    function getMd() {
        return $this->md;
    }

    function getAg() {
        return $this->ag;
    }

    function getClase() {
        return $this->clase;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setRaza($raza) {
        $this->raza = $raza;
    }

    function setHp($hp) {
        $this->hp = $hp;
    }

    function setMn($mn) {
        $this->mn = $mn;
    }

    function setStr($str) {
        $this->str = $str;
    }

    function setMd($md) {
        $this->md = $md;
    }

    function setAg($ag) {
        $this->ag = $ag;
    }

    function setClase($clase) {
        $this->clase = $clase;
    }

    public function attack() {
        // no se realiza el cáculo del daño de ataque y no recibe un objeto al que atacar
        return $this->getNombre()." ataca a ".$this->getNombre()." causando "." de daño";
    }
    public function getHurt($hp) {
        //No se define la implementación
    }
    public function dramaticDeath($hp) {
        return "Arrrg! yo ".$this->getNombre()." he sido derrotado en batalla.";
    }
}
