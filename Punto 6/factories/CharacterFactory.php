<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CharacterFactory
 *
 * @author Kev
 */
class CharacterFactory implements ICharacterFactory{
    //put your code here
    public function getCharacterWarrior($nombre, $raza, $hp, $mn, $str, $md, $ag) : \Warrior{
        return new Warrior($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }
    public function getCharacterMage($nombre, $raza, $hp, $mn, $str, $md, $ag) : \Mage{
        return new Mage($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }
    public function getCharacterRogue($nombre, $raza, $hp, $mn, $str, $md, $ag) : \Rogue{
        return new Rogue($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }
}
